# DHCP Starvation

The application crafts DHCP messages to use up the whole address pool of a legitimate DHCP server. Thus no new client can get a new address. More details can be found in the documentation (in Czech).

## Running the application:

~~~
./pds-dhcpstarve -i interface
~~~

(interface (string) should be a name of the interface (according to OS) where the attacker generates the malicious traffic)