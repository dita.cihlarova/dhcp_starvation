LOGIN=xcihla02
SOURCE_CODE=main.cpp Makefile
DOCS=dokumentace.pdf readme
ALLFILES=$(SOURCE_CODE) $(DOCS)
PACK=$(LOGIN).zip

all: pds-dhcpstarve
#	./pds-dhcpstarve

pds-dhcpstarve: main.cpp
	g++ -std=c++11 -o $@ $<

.PHONY: all clean pack

clean:
	rm -f pds-dhcpstarve $(PACK)

pack: $(ALLFILES)
	zip $(PACK) $(ALLFILES)
